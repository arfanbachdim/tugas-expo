import React, { Component } from 'react'
import { View, Image, StyleSheet, Text, TextInput } from 'react-native'

export default class App extends Component {
  render() {
    return (
        <View style={styles.container}>
      <Image
        style={styles.image}
        source={require('./assets/logo.png')}
      />
      <Text style={styles.text}>Login</Text>
      <Text style={styles.text1}>Username / Email</Text>
      <TextInput style={styles.textbox} onChangeText={(text) => this.setState({ value: text })}></TextInput>
      <Text style={styles.text1}>Password</Text>
      <TextInput style={styles.textbox} onChangeText={(text) => this.setState({ value: text })}></TextInput>
      <Text style={styles.box1}>Masuk</Text>
      <Text style={styles.box1}>Daftar ?</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        //backgroundColor: '#E5E5E5',
      },
    image: {
        width: 375,
        height: 102,
        left: 0,
        top: 63
    },
    text: {
        //backgroundColor: 'whitesmoke',
        //padding: 10,
        //position: absolute,
        width: 60,
        height: 28,
        left: 158,
        top: 110,
        //fontFamily: Roboto,
        //fontStyle: normal,
        //fontWeight: normal,
        fontSize: 24,
        lineHeight: 28,
        color: '#003366',
    },
    text1: {
        //position: absolute,
        padding: 0,
        width: 127,
        height: 19,
        left: 41,
        top: 180,
        
        //fontFamily: Roboto,
        //fontStyle: normal,
        //fontWeight: normal,
        fontSize: 16,
        lineHeight: 19,
        //color: '#003366',
        /* identical to box height */
    },
    textbox: {
        //position: absolute,
        width: 294,
        height: 48,
        left: 41,
        top: 180,

        //background: '#FFFFFF',
        //borderColor: '#003366', 
        borderWidth: 1,
        //boxSizing: border-box,
    },
    box1: {
        //position: absolute,
        width: 140,
        height: 40,
        left: 118,
        top: 200,
        textAlign: "center",
        textAlignVertical:"center",
        fontSize:18,
        marginTop: 15,
        backgroundColor: '#3EC6FF',
        borderRadius: 16,
    }      
        
        
})