import React, { Component } from 'react'
import { View, Image, StyleSheet, Text, TextInput } from 'react-native'

export default class App extends Component {
  render() {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Tentang Saya</Text>
            <Text style={styles.box1}></Text>
            <Text style={styles.text1}>Ayun Fadli</Text>
            <Text style={styles.text2}>React Native</Text>
            <Text style={styles.textbox}></Text>
            
        </View>
    )
  }
}

const styles = StyleSheet.create({
    image: {
        width: 375,
        height: 102,
        left: 0,
        top: 63
    },
    text: {
        textAlign: "center",
        width: 250,
        height: 42,
        left: 55,
        top: 64,

        //fontFamily: Roboto,
        //fontStyle: normal,
        //fontWeight: bold,
        fontSize: 36,
        lineHeight: 42,
        color: '#003366',
    },

    box1: {
        backgroundColor: '#EFEFEF',
        width: 200,
        height: 200,
        left: 80,
        top: 118,
        textAlign: "center",
        textAlignVertical:"center",
        fontSize:18,
        marginTop: 15,
        //backgroundColor: '#3EC6FF',
        borderRadius: 16,
    },
    
    text1: {
        width: 250,
        height: 42,
        left: 55,
        top: 130,
        textAlign: "center",
        textAlignVertical:"center",

        //fontFamily: Roboto,
        //fontStyle: normal,
        //fontWeight: bold,
        fontSize: 26,
        lineHeight: 42,
        color: '#003366',
    },


    text2: {
        width: 168,
        height: 19,
        left: 100,
        top: 130,

        //font-family: Roboto;
        //font-style: normal;
        //font-weight: bold;
        textAlign: "center",
        textAlignVertical:"center",
        fontSize: 16,
        lineHeight: 19,
        /* identical to box height */


        color: '#3EC6FF',
    },
    textbox: {
        //position: absolute;
        width: 130,
        height: 90,
        left: 25,
        right: 3.22,
        top: 170,
        bottom: 6.51,

        backgroundColor: '#3EC6FF'
    },

    textbox2: {
        //position: absolute;
        width: 130,
        height: 90,
        left: 25,
        right: 3.22,
        top: 170,
        bottom: 6.51,

        backgroundColor: '#3EC6FF'
    }
            
        
})